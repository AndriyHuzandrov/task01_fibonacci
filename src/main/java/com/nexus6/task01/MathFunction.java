package com.nexus6.task01;

/**
 * <h1>MathFunction</h1>
 * This class contains methods for data interval processing.
 * No properties are needed
 *
 *
 * @author  Andriy Huzandrov
 * @version 1.0
 * @since   2019-03-31
 */
class MathFunction {
  /**
   * This is an utility method is used to check whether an argument is
   * even or not.
   * @param num This is the parameter to check
   * @return boolean true if the argument is even and false for odd argument.
   */
  private boolean testNumEven(int num) {
    return num % 2 == 0;
  }
  /**
   * This method is used to print out odd numbers from an array,
   * which is passed as parameter.
   * @param arr This is array of integers.
   */
  void printOdds(int[] arr) {
    int numsAtRow = 10; // for formating output
    int count = 0; // used only for formating the method output
    for (int n : arr) {
      if (n != 0 && !testNumEven(n)) {
        System.out.printf("%5d", n);
        count++;
      }
      if (count % numsAtRow == 0) {
        System.out.println();
      }
    }
    System.out.println();
  }
  /**
   * This method is used to print out even numbers from an array,
   * which is passed as parameter.
   * @param arr This is array of integers.
   */
  void printEvens(int[] arr) {
    int numsAtRow = 10; // for formating output
    int count = 0; // for formating output
    for (int i = arr.length - 1; i >= 0; i--) {
      if (arr[i] != 0 && testNumEven(arr[i])) {
        System.out.printf("%5d", arr[i]);
        count++;
      }
      if (count % numsAtRow == 0) { System.out.println(); }
    }
    System.out.println();
  }
  /**
   * This method is used to print out the sum of
   * all odd numbers from an array, which is passed as parameter.
   * @param arr This is array of integers.
   */
  void calcOddsSum(int[] arr) {
    int sum = 0;
    for (int n : arr) {
      if (testNumEven(n)) {
        sum += n;
      }
    }
    System.out.println("\nSum of odd numbers = " + sum);
  }
  /**
   * This method is used to print out the sum of
   * all even numbers from an array, which is passed as parameter.
   * @param arr This is array of integers.
   */
  void calcEvensSum(int[] arr) {
    int sum = 0;
    for (int n : arr) {
      if (!testNumEven(n)) {
        sum += n;
      }
    }
    System.out.println("\nSum of even numbers = " + sum);
  }
  /**
   * This method is used to build a Fibonacci row taking as base
   * numbers max odd and even numbers from an array, which is passed
   * as parameter.
   * @param arr This is array of integers.
   * @param size denotes the size of Fibonacci's row.
   * @return an array which encapsulates Fibonacci's row
   */
  long[] biuldFibanacci(int[] arr, int size) {
    long[] outputFebArr = new long[size];
    long f1;
    long f2;
    if (testNumEven(arr[arr.length - 1])) {
      f2 = arr[arr.length - 1];
      f1 = f2 - 1;
    } else {
      f1 = arr[arr.length - 1];
      f2 = f1 - 1;
    }
    outputFebArr[0] = f1;
    outputFebArr[1] = f2;
    for (int i = 2; i < outputFebArr.length; i++) {
      outputFebArr[i] = outputFebArr[i - 1] + outputFebArr[i - 2];
    }
    for (long n : outputFebArr) {
      System.out.println(n);
    }
    return outputFebArr;
  }
  /**
   * This method calculates percentage amount of odd and even numbers
   * in previously  built Fibonacci row taking as base
   * numbers max odd and even numbers from an array, which is
   * passed as parameter.
   * @param arr This is array of integers.
   */
  void showPercentage(long[] arr) {
    int countEven = 0;
    int countOdd = 0;
    double percent = 100.0;
    double oddPerc;
    double evenPerc;
    for (long n : arr) {
      if (testNumEven((int) n)) {
        countEven++;
      }
      else {
        countOdd++;
      }
    }
    oddPerc = countOdd * percent / arr.length;
    evenPerc = countEven * percent / arr.length;
    System.out.printf("Percent of odd numbers = %5.2f%%%n"
                      + "Percent of even numbers = %5.2f%%%n",
                      oddPerc, evenPerc);
  }
}
