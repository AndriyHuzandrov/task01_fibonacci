package com.nexus6.task01;

/**
 * <h1>NumbersHolder</h1>
 * This class stores interval of numbers in the arrays and also contains methods for data interval
 * processing.
 *
 * @author Andriy Huzandrov
 * @version 1.0
 * @since 2019-03-31
 */
class NumbersHolder {

  private int startBound;
  private int endBound;
  private long[] febNumArr; //array for default size of feb calculation
  private int[] workingNumArr;

  /**
   * This is an only one class constructor. Default class constructor is not stipulated. As a
   * parameter an array consisting of interval bounds is passed to the constructor, which then
   * explodes for setting utility method is used to check whether an argument is even or not.
   * @param bounds an array holding start and end of number interval
   */
  NumbersHolder(int[] bounds) {
    startBound = bounds[0];
    endBound = bounds[1];
    workingNumArr = new int[endBound - startBound + 1];
    fillArr();
  }

  /**
   * This method takes a menu item index and perform a respective call of math method to process
   * data. is an utility method is used to check whether an argument is
   *
   * @param choice This is the parameter which denotes a main menu item
   */
  void execChoice(int choice) {
    MathFunction math = new MathFunction();
    switch (choice) {
      case 1:
        math.printOdds(workingNumArr);
        break;
      case 2:
        math.printEvens(workingNumArr);
        break;
      case 3:
        math.calcOddsSum(workingNumArr);
        break;
      case 4:
        math.calcEvensSum(workingNumArr);
        break;
      case 5:
        int size;
        MenuGenerator menu = new MenuGenerator();
        do {
          size = menu.getFebanacciRowSize();
        } while (!menu.getInputStatus());

        febNumArr = math.biuldFibanacci(workingNumArr, size);
        break;
      case 6:
        if (febNumArr == null) {
          int s;
          MenuGenerator m = new MenuGenerator();
          do {
            s = m.getFebanacciRowSize();
          } while (!m.getInputStatus());

          febNumArr = math.biuldFibanacci(workingNumArr, s);
          math.showPercentage(febNumArr);
        }
        else {
          math.showPercentage(febNumArr);
        }
        break;
      default:
        break;
    }
  }

  /**
   * This method fills an instance array with all numbers from the interval selected by user for
   * further processing.
   */
  private void fillArr() {
    for (int i = startBound; i < endBound + 1; i++) {
      workingNumArr[i - startBound] = i;
    }
  }
}
