package com.nexus6.task01;

import java.io.*;

/**
 * <h1>NMenuGenerator</h1>
 * This class implements simple  console UI.
 * All input data are checked for type and
 * consistency
 *
 * @author  Andriy Huzandrov
 * @version 1.0
 * @since   2019-03-31
 */

class MenuGenerator {
  /** The property holds order index of the first main menu item */
  private static final int FIRST_ITEM = 1;
  /** The property holds order index of the last main menu item */
  private static final int LAST_ITEM = 6;
  /** for storing  welcome UI message */
  private String wellcomeMsg;
  /** for storing main menu  item messages of UI  */
  private String[] menuItems = new String[LAST_ITEM];
  /**  stores status of an correct data input*/
  private boolean correctInput = true;
  /**
   * This is default and an only one class constructor.
   * Const string menu items are written to the array
   *
   */
  MenuGenerator() {
    wellcomeMsg = "\nEnter interval bounds like \"start:end\" or \"exit\" > ";
    menuItems[0] = "Print odd numbers from the defined interval";
    menuItems[1] = "Print even numbers from the defined interval in reverse order";
    menuItems[2] = "Print a sum of odd numbers from the defined interval";
    menuItems[3] = "Print a sum of even numbers from the defined interval";
    menuItems[4] = "Build Fibonacci sieries";
    menuItems[5] = "Print odd/even percentage in the Fibonacci sieries";
  }
  /**
   * This method returns the value of
   * class field.
   *
   * @return boolean true if input data are ok
   */
  boolean getInputStatus() {
    return correctInput;
  }
  /**
   * This method generates UI message.
   */
  void showWellcomeMsg() {
    System.out.print(wellcomeMsg);
  }
  /**
   * This method generates UI message.
   */
  void showMainMenu() {
    System.out.println();
    for (int i = 0; i < menuItems.length; i++) {
      System.out.printf("%d : %s%n", i + 1, menuItems[i]);
    }
    System.out.print("Choose action or 0 for exit: ");
  }
  /**
   * This method takes an input string and transform it
   * to the array with int bounds of interval
   * No exception will be thrown - data validation was
   * performed before.
   * @param str This is the parameter which contains string
   * with proper user defined interval bounds
   * @return bounds are transformed to an array and then
   * it is returned.
   */
  private int[] strToIntArr(String str) {
    int i = 0;
    int[] testArr = new int[2];
    for (String s : str.split(":")) {
      testArr[i] = Integer.valueOf(s);
      i++;
    }
    return testArr;
  }
  /**
   * This method verifies input data by three filters
   * depending on menu data acquired from.
   * (may be transformed to return boolean type)
   *
   * @param inpStr input string containing unverified data
   * @param mode indicates what type of verification
   * to perform.
   */
  private void checkInput(String inpStr, char mode) { //mode to select which menu takes an input
    boolean checked = true;
    int[] arr;

    if (inpStr.compareTo("exit") == 0) {
      System.exit(0);
    }

    switch(mode) {
      case 'w':
        if (!inpStr.matches("\\d*:\\d*")) {
          checked = false;
        }
        else {
          arr = this.strToIntArr(inpStr);
          if((arr[1] <= arr[0]) || (arr[0] < 0)) {
            checked = false;
          }
        }
        break;
      case 'm':
        if (!inpStr.matches("\\d")) {
          checked = false;
        }
        else if (Integer.valueOf(inpStr) == 0) {
          System.exit(0);
        }
        else if ((Integer.valueOf(inpStr) < FIRST_ITEM)
            || (Integer.valueOf(inpStr) > LAST_ITEM)) {
          checked = false;
        }
        break;
      default:
        if (!inpStr.matches("\\d*")) {
          checked = false;
        }
        else if (Integer.valueOf(inpStr) == 0) {
          checked = false;
        }
        break;
    }
    this.correctInput =  checked;
  }
  /**
   * This method forms an array with
   * user defined bounds
   */
  int[] getIntervalBounds() {
    String inpStr;
    int[] outArr = new int[2];
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    try {
      inpStr = br.readLine();
      this.checkInput(inpStr, 'w');
      if (!this.getInputStatus()) {
        throw new NumberFormatException();
      }
      else {
        outArr = this.strToIntArr(inpStr);
      }
    }
    catch (IOException e) {
      System.out.println("\nRead Error");
    }
    catch (NumberFormatException exc) {
      System.out.printf("\nWrong interval bounds. Must be from 0 to %5d",
          Integer.MAX_VALUE);
    }

    return outArr;
  }
  /**
   * This method takes user chosen
   * menu item and passes it to model part
   * NumbersHolder instance
   *
   * @return  menu item index
   */
  int getMenuChoice() {
    String inpStr;
    int choice = -1;
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      inpStr = br.readLine();
      this.checkInput(inpStr, 'm');
      if (!this.getInputStatus()) {
        throw new NumberFormatException();
      }
      else {
        choice = Integer.valueOf(inpStr);
      }
    }
    catch (IOException e) {
      System.out.println("Read Error");
    }
    catch (NumberFormatException exc) {
      System.out.println("\nNo such menu item");
    }
    return choice;// value is passed to math class
  }
  /**
   * This method takes user chosen
   *size of Fibonacci series and
   * passes it to a caller
   *
   * @return  Fibonacci numbers row size
   */
  int getFebanacciRowSize(){
    int row_size = 10;
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    System.out.print("Enter Febanacci row size : ");
    try {
      String inpStr = br.readLine();
      this.checkInput(inpStr, 'f');
      if (!this.getInputStatus()) {
        throw new NumberFormatException();
      }
      else {
        row_size = Integer.valueOf(inpStr);
      }
    }
    catch (IOException e) {
      System.out.println("Read Error");
    }
    catch (NumberFormatException exc) {
      System.out.println("\nWrong value of the size");
    }
    return row_size;
  }
}
