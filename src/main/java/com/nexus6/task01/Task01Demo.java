package com.nexus6.task01;

/**
 * <h1>Task01Demo</h1>
 * This module contains entry point and facilitate
 * to launch whole project.
 * Menus are generated and is main and only thread
 * to run whole application
 *
 * @author  Andriy Huzandrov
 * @version 1.0
 * @since   2019-03-31
 */

class Task01Demo {
  /**
   * Entry point
   *
   */
  public static void main(String[] args) {
    int[] boundsArr;
    int menuChoice;
    MenuGenerator menu = new MenuGenerator();
    NumbersHolder data;

    do {
      menu.showWellcomeMsg();
      boundsArr = menu.getIntervalBounds();
    } while (!menu.getInputStatus());
    data = new NumbersHolder(boundsArr);

    while (menu.getInputStatus()) {
      do {
        menu.showMainMenu();
        menuChoice = menu.getMenuChoice();
      } while (!menu.getInputStatus());
      data.execChoice(menuChoice);
    }
  }
}
